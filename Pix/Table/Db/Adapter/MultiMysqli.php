<?php

/**
 * Pix_Table_Db_Adapter_MultiMysqli 
 * 
 * @uses Pix_Table_Db_Adapter
 * @package Table
 * @copyright 2003-2012 PIXNET Digital Media Corporation
 * @license http://framework.pixnet.net/license BSD License
 */
class Pix_Table_Db_Adapter_MultiMysqli extends Pix_Table_Db_Adapter_MysqlCommon
{
    protected $master_db_config;
    protected $slave_db_config;
    protected $master_link;
    protected $slave_link;

    protected $link;
    protected static $for_update_enable;

    public function __construct(array $master_db_config, array $slave_db_config)
    {
        self::$for_update_enable = false;

        if (empty($master_db_config) or empty($slave_db_config)) {
            throw new Exception('Please provide master and slave configs both');
        }

        $this->master_db_config = $master_db_config;
        $this->slave_db_config = $slave_db_config;

        $this->master_link = $this->connect($master_db_config);
        $this->slave_link = $this->connect($slave_db_config);
        $this->link = $this->master_link;
    }

    protected function connect(array $db_config)
    {
        $conn = new mysqli(
            $db_config['host'],
            $db_config['user'],
            $db_config['password'],
            $db_config['database']
        );
        if ($conn->connect_error) {
            throw new Exception('unable to connect ' . $db_config['host'] . ' with ' . $db_config['user']);
        }
        $conn->set_charset('utf8');
        return $conn;
    }

    public function startForUpdate()
    {
        self::$for_update_enable = true;
    }

    public function stopForUpdate()
    {
        self::$for_update_enable = false;
    }

    public function getSupportFeatures()
    {
        return array('force_master', 'immediate_consistency', 'check_table');
    }

    /**
     * __get 為了與 MySQLi object 相容所加的 __get($name); 
     * 
     * @param mixed $name 
     * @access public
     * @return void
     */
    public function __get($name)
    {
        return $this->link->{$name};
    }

    /**
     * __call 為了與 MySQLi 相容所加的 __call()
     * 
     * @param mixed $name 
     * @param mixed $args 
     * @access public
     * @return void
     */
    public function __call($name, $args)
    {
        return call_user_func_array(array($this->link, $name), $args);
    }

    protected function determineLinkAndSQL(string $sql): string
    {
        // critical read enabled
        if (Pix_Table::$_force_master or self::$for_update_enable) {
            $is_master_link_used = true;
            if (preg_match('#^SELECT #', strtoupper($sql))) {
                $sql .= ' FOR UPDATE';
            }
        } else {
            $is_master_link_used = !preg_match('#^SELECT #', strtoupper($sql));
        }

        $this->link = ($is_master_link_used) ? $this->master_link : $this->slave_link;

        // 檢查是否連線
        if (!$this->link->ping()) {
            if ($is_master_link_used) {
                $this->master_link = $this->connect($this->master_db_config);
                $this->link = $this->master_link;
            } else {
                $this->slave_link = $this->connect($this->slave_db_config);
                $this->link = $this->slave_link;
            }
        }

        return $sql;
    }

    /*
        支援 mysqli statment 的處理方式
    */
    public function prepare($sql)
    {
        $sql = $this->determineLinkAndSQL($sql);
        return $this->link->prepare($sql);
    }

    /**
     * query 對 db 下 SQL query
     * 
     * @param mixed $sql 
     * @access protected
     * @return Mysqli result
     */
    public function query($sql, $table = null)
    {
        global $lock_debug;

        $sql = $this->determineLinkAndSQL($sql);

        if ($lock_debug) {
            global $lock_debug_sql;
            $lock_debug_sql .= $sql . "\n";
        }

        if (Pix_Table::$_log_groups[Pix_Table::LOG_QUERY]) {
            $short_sql = mb_strimwidth($sql, 0, 512, "...len=" . strlen($sql));
            Pix_Table::debug(sprintf("[%s]\t%40s", $this->link->host_info, $short_sql));
        }

        $start_time = microtime(true);
        $res = $this->link->query($sql);
        if (($t = Pix_Table::getLongQueryTime()) and ($delta = (microtime(true) - $start_time)) > $t) {
            Pix_Table::debug(sprintf("[%s]\t%s\t%40s", $this->link->host_info, $delta, $sql));
        }

        if ($res === false) {
            if ($errno = $this->link->errno) {
                switch ($errno) {
                    case 1062:
                        throw new Pix_Table_DuplicateException($this->link->error, $errno);
                    case 1406:
                        throw new Pix_Table_DataTooLongException($this->link->error, $errno);
                    default:
                        throw new Exception("SQL Error: {$this->link->error} SQL: $sql");
                }
            }
        }
        return $res;
    }

    /**
     * quote 將 $str 字串內容 quote 起來。
     * 
     * @param string $str 
     * @access public
     * @return string
     */
    public function quoteWithColumn($table, $value, $column_name)
    {
        if (is_null($column_name)) {
            return "'" . $this->link->real_escape_string(strval($value)) . "'";
        } 

        if ($table->isNumbericColumn($column_name)) {
            return intval($value);
        }

        if (!is_scalar($value)) {
            trigger_error("{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']} 的 column `{$column_name}` 格式不正確: " . gettype($value), E_USER_WARNING);
        }
        return "'" . $this->link->real_escape_string(strval($value)) . "'";
    }

    public function getLastInsertId($table)
    {
        return $this->link->insert_id;
    }
}
